

import org.furman.CelsiusToFahrenheitConverter;
import org.furman.CelsiusToKelvinConverter;
import org.furman.TemperatureConverter;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class TemperatureConversionExampleTest {

    @Test
    public void testCelsiusToFahrenheitConversion() {
        TemperatureConverter fahrenheitConverter = new CelsiusToFahrenheitConverter();
        double celsius = 25;
        double expectedFahrenheit = 77;
        double actualFahrenheit = fahrenheitConverter.fromCelsius(celsius);
        assertEquals(expectedFahrenheit, actualFahrenheit, 0.001);
    }

    @Test
    public void testCelsiusToKelvinConversion() {
        TemperatureConverter kelvinConverter = new CelsiusToKelvinConverter();
        double celsius = 25;
        double expectedKelvin = 298.15;
        double actualKelvin = kelvinConverter.fromCelsius(celsius);
        assertEquals(expectedKelvin, actualKelvin, 0.001);
    }
}
