package com.furman;

import org.furman.CelsiusToFahrenheitConverter;
import org.furman.CelsiusToKelvinConverter;
import org.furman.TemperatureConverter;

public class TemperatureConversionExample {
    public static void main(String[] args) {
        double celsiusTemperature = 25.0;


        TemperatureConverter celsiusToFahrenheit = new CelsiusToFahrenheitConverter();
        double fahrenheitTemperature = celsiusToFahrenheit.fromCelsius(celsiusTemperature);
        System.out.println(celsiusTemperature + " °C = " + fahrenheitTemperature + " °F");


        TemperatureConverter celsiusToKelvin = new CelsiusToKelvinConverter();
        double kelvinTemperature = celsiusToKelvin.fromCelsius(celsiusTemperature);
        System.out.println(celsiusTemperature + " °C = " + kelvinTemperature + " K");
    }
}