package org.furman;

public class CelsiusToKelvinConverter extends TemperatureConverter {
    @Override
    public double toCelsius(double temperature) {
        return temperature - 273.15;
    }

    @Override
    public double fromCelsius(double celsius) {
        return celsius + 273.15;
    }

    @Override
    public String getScaleName() {
        return "K";
    }
}
