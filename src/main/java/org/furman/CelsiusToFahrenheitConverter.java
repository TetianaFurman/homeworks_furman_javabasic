package org.furman;

public class CelsiusToFahrenheitConverter extends TemperatureConverter {
    @Override
    public double toCelsius(double temperature) {
        return (temperature - 32) / 1.8;
    }

    @Override
    public double fromCelsius(double celsius) {
        return celsius * 1.8 + 32;
    }

    @Override
    public String getScaleName() {
        return "°F";
    }
}

